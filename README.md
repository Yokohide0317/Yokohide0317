```
class Yoko(){

    langs = ["Python", "JS", "GAS", "Bash"]

    def __init__(self):
        self.name = "Yokohide"
        self.age = 19
        self.country = "JP"
        self.main_lang = langs[0] 
    
    def hello(self):
        msg = "Hi!. I'm %s, %s years old. From %s and I do %s" % (self.name, self.age, self.country, self.main_lang.)
        return msg
}
```


<img src="https://i1.wp.com/nobon.me/wp-content/uploads/2016/02/tumblr_o1lcumzQ0P1uf9lyxo1_500.gif?fit=500%2C315&ssl=1">
<!--
**Yokohide0317/Yokohide0317** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
